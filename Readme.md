## General
This code is based on [jackson-modules-java8](https://github.com/FasterXML/jackson-modules-java8)


## License

All modules are licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt).
