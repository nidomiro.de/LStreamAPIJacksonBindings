package de.nidomiro.streambindings;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;

/**
 * Created by Niclas Roßberger on 05.11.17.
 */

public class StreamModule extends Module {
    @Override
    public String getModuleName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public Version version() {
        return Version.unknownVersion();
    }

    @Override
    public void setupModule(SetupContext context) {
        context.addSerializers(new StreamSerializers());
        context.addDeserializers(new StreamDeserializers());

        context.addTypeModifier(new StreamTypeModifier());
    }
}
